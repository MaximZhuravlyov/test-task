import React, { useEffect } from 'react';
import { useLocation } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import Loader from 'react-loader-spinner';

import * as starshipsReducer from '../store/reducers/starships';
import { StarshipType } from '../components/Starship/types';
import { getStarship } from '../store/actions/starships/getStarship';

import Header from '../components/Header';
import StarshipComponent from '../components/StarshipComponent';

const Starship: React.FC = (): JSX.Element => {
  const location = useLocation();
  const dispatch = useDispatch();
  const starship: StarshipType = useSelector(starshipsReducer.state.starship.data);
  const status: string = useSelector(starshipsReducer.state.starship.status);

  useEffect(() => {
    dispatch(getStarship.get(location.state.starship.url));
  }, []);

  if (status === 'pending' || status === null) {
    return (
      <div className="spinner">
        <Loader
          type="Puff"
          color="#00BFFF"
          height={100}
          width={100}
        />
      </div>
    );
  } else {
    return (
      <>
        <Header />
        <StarshipComponent starship={starship} />
      </>
    );
  }
}

export default Starship;
