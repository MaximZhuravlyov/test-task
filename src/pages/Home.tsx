import React from 'react';

import Header from '../components/Header';
import HomeComponent from '../components/Home';

const Home: React.FC = (): JSX.Element => {
  return (
    <>
      <Header />
      <HomeComponent />
    </>
  )
}

export default Home;
