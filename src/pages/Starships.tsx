import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Loader from 'react-loader-spinner';

import * as starshipsReducer from '../store/reducers/starships';
import { getStarships } from '../store/actions/starships/getStarships';
import { StarshipType } from '../components/Starship/types';

import Header from '../components/Header';
import StarshipsComponent from '../components/Starships';

const Starships: React.FC = (): JSX.Element => {
  const dispatch = useDispatch();
  const starships: StarshipType[] = useSelector(starshipsReducer.state.starships.data);
  const status: string = useSelector(starshipsReducer.state.starships.status);

  useEffect(() => {
    dispatch(getStarships.get());
  }, []);

  if (status === 'pending' || status === null) {
    return (
      <div className="spinner">
        <Loader
          type="Puff"
          color="#00BFFF"
          height={100}
          width={100}
        />
      </div>
    );
  } else {
    return (
      <>
        <Header />
        <StarshipsComponent data={starships} />
      </>
    );
  }
}

export default Starships;
