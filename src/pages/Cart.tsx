import React from 'react';

import Header from '../components/Header';
import CartComponent from '../components/Cart';

const Cart: React.FC = (): JSX.Element => {
  return (
    <>
      <Header />
      <CartComponent />
    </>
  );
}

export default Cart;
