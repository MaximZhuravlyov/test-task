import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Loader from 'react-loader-spinner';

import * as peopleReducer from '../store/reducers/people';
import { getPeople } from '../store/actions/people/getPeople';
import { PersonType } from '../components/Person/types';

import Header from '../components/Header';
import PeopleComponent from '../components/People';

const People: React.FC = (): JSX.Element => {
  const dispatch = useDispatch();
  const people: PersonType[] = useSelector(peopleReducer.state.people.data);
  const status: string = useSelector(peopleReducer.state.people.status);

  useEffect(() => {
    dispatch(getPeople.get());
  }, []);

  if (status === 'pending' || status === null) {
    return (
      <div className="spinner">
        <Loader
          type="Puff"
          color="#00BFFF"
          height={100}
          width={100}
        />
      </div>
    );
  } else {
    return (
      <>
        <Header />
        <PeopleComponent data={people} />
      </>
    );
  }
}

export default People;
