import React, { useEffect } from 'react';
import { useLocation } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import Loader from 'react-loader-spinner';

import * as peopleReducer from '../store/reducers/people';
import { PersonType } from '../components/Person/types';
import { getPerson } from '../store/actions/people/getPerson';

import Header from '../components/Header';
import PersonComponent from '../components/PersonComponent';

const Person: React.FC = (): JSX.Element => {
  const location = useLocation();
  const dispatch = useDispatch();
  const person: PersonType = useSelector(peopleReducer.state.person.data);
  const status: string = useSelector(peopleReducer.state.person.status);

  useEffect(() => {
    dispatch(getPerson.get(location.state.person.url));
  }, []);

  if (status === 'pending' || status === null) {
    return (
      <div className="spinner">
        <Loader
          type="Puff"
          color="#00BFFF"
          height={100}
          width={100}
        />
      </div>
    );
  } else {
    return (
      <>
        <Header />
        <PersonComponent person={person} />
      </>
    );
  }
}

export default Person;
