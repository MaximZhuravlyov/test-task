import React from 'react';
import { Route, Routes } from 'react-router-dom';

import './global.scss';

import Home from './pages/Home';
import People from './pages/People';
import Person from './pages/Person';
import Starships from './pages/Starships';
import Starship from './pages/Starship';
import Cart from './pages/Cart';

const Router: React.FC = (): JSX.Element => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/people" element={<People />} />
      <Route path="/people/:name" element={<Person />} />
      <Route path="/starships" element={<Starships />} />
      <Route path="/starships/:name" element={<Starship />} />
      <Route path="/cart" element={<Cart />} />
    </Routes>
  );
}

export default Router;
