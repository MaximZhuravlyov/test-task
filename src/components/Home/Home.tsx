import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import './Home.scss';

import { PersonType } from '../Person/types';
import { StarshipType } from '../Starship/types';
import { getPeople } from '../../store/actions/people/getPeople';
import { getStarships } from '../../store/actions/starships/getStarships';

const Home: React.FC = (): JSX.Element => {
  const dispatch = useDispatch();

  const people: PersonType[] = useSelector((state: any) => state.people.people);
  const starships: StarshipType[] = useSelector((state: any) => state.starships.starships);

  useEffect(() => {
    dispatch(getPeople.get());
    dispatch(getStarships.get());
  }, []);

  return (
    <main className="home">
      <section className="container home__container">
        <Link to="/people" className="home__people">
          People: {people.length}
        </Link>
        <Link to="/starships" className="home__starships">
          Starships: {starships.length}
        </Link>
      </section>
    </main>
  );
}

export default Home;
