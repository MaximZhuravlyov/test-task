import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import * as cartReducer from '../../store/reducers/cart';
import { addToCart } from '../../store/actions/cart';
import { removeFromCart } from '../../store/actions/cart';

import './Person.scss';

import { PersonType } from './types';

interface Props {
  person: PersonType;
  isPointer: boolean;
  isInCart?: boolean;
}

const Person: React.FC<Props> = ({ person, isPointer, isInCart }): JSX.Element => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const className = isPointer ? 'person person--pointer' : 'person';
  const url = person.url.split('/')[5];
  const isAddedToCart: boolean = useSelector(cartReducer.state.cart.data).some((item: any) => item.item.url === person.url);

  return (
    <li
      className={className}
      key={person.name}
    >
      <p
        className="person__name"
        onClick={() => {
          navigate(`/people/${url}`, {
            state: {
              person
            }
          });
        }}
      >
        {person.name} ({person.gender}, {person.birth_year})
      </p>
      <p>Height: {person.height} cm</p>
      <p>Mass: {person.mass} kg</p>
      <p>Hair color: {person.hair_color}</p>
      <p>Skin color: {person.skin_color}</p>
      <p>Homeworld: {person.homeworld}</p>

      {!isAddedToCart ? (
        <button
          type="button"
          className="person__add-to-cart"
          onClick={() => {
            dispatch(addToCart.add({
              item: person,
              type: 'person'
            }));
          }}
        >
          Add to cart
        </button>
      ) : !isInCart ? (
        <p className="person__added-to-cart">
          Added to the cart
        </p>
      ) : (
        <p
          className="person__remove-from-cart"
          onClick={() => {
            dispatch(removeFromCart.remove({
              item: person,
              type: 'person'
            }));
          }}
        >
          Remove from the cart
        </p>
      )}
    </li>
  );
}

export default Person;
