import React, { useState, useEffect } from 'react';

import './People.scss';

import { PersonType } from '../../components/Person/types';

import Person from '../../components/Person';

interface Props {
  data: PersonType[];
}

const People: React.FC<Props> = ({ data }): JSX.Element => {
  const [ mode, setMode ] = useState<string | null>('');

  let view: string | null = localStorage.getItem('people-view') ? localStorage.getItem('people-view') : 'table';

  useEffect(() => {
    setMode(view);
  }, []);

  return (
    <main className="people">
      <section className="container people__container">
        <ul className="people__view">
          <li>
            <button
              className="people__view-table"
              onClick={() => {
                setMode('table');
                localStorage.setItem('people-view', 'table');
              }}
            >
              <i className={`people__view-table-rectangle ${mode === 'table' ? ' people__view-table-rectangle--active' : ''}`}></i>
              <i className={`people__view-table-rectangle ${mode === 'table' ? ' people__view-table-rectangle--active' : ''}`}></i>
              <i className={`people__view-table-rectangle ${mode === 'table' ? ' people__view-table-rectangle--active' : ''}`}></i>
              <i className={`people__view-table-rectangle ${mode === 'table' ? ' people__view-table-rectangle--active' : ''}`}></i>
            </button>
          </li>
          <li>
            <button
              className={`people__view-list${mode === 'list' ? ' people__view-list--active' : ''}`}
              onClick={() => {
                setMode('list');
                localStorage.setItem('people-view', 'list');
              }}
            >
              <i className={`people__view-list-line ${mode === 'list' ? ' people__view-list-line--active' : ''}`}></i>
              <i className={`people__view-list-line ${mode === 'list' ? ' people__view-list-line--active' : ''}`}></i>
              <i className={`people__view-list-line ${mode === 'list' ? ' people__view-list-line--active' : ''}`}></i>
            </button>
          </li>
        </ul>
        <h1 className="people__title">
          People
        </h1>
        <ul className={`people__${mode}`}>
          {data.map((item: PersonType) => (
            <Person
              person={item}
              isPointer={true}
              key={item.name}
            />
          ))}
        </ul>
      </section>
    </main>
  );
}

export default People;
