import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';

import * as cartReducer from '../../store/reducers/cart';

import './Header.scss';

const Header = () => {
  const cart: Array<any> = useSelector(cartReducer.state.cart.data);

  return (
    <header className="header">
      <div className="container">
        <div className="header__inner">
          <nav className="header__nav">
            <NavLink
              to="/"
              className={(navData) => navData.isActive ? 'header__link header__link--active' : 'header__link'}
            >
              Home
            </NavLink>
            <NavLink
              to="/people"
              className={(navData) => navData.isActive ? 'header__link header__link--active' : 'header__link'}
            >
              People
            </NavLink>
            <NavLink
              to="/starships"
              className={(navData) => navData.isActive ? 'header__link header__link--active' : 'header__link'}
            >
              Starships
            </NavLink>
          </nav>
          <div className="header__cart">
            <NavLink
              to="/cart"
              className={(navData) => navData.isActive ? 'header__link header__link--cart header__link--active' : 'header__link header__link--cart'}
            >
              Cart ({cart.length})
              <div className="header__cart-dropdown">
                <p>
                  People: {cart.filter((item: any) => item.type === 'person').length}
                </p>
                <p>
                  Starships: {cart.filter((item: any) => item.type === 'starship').length}
                </p>
              </div>
            </NavLink>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
