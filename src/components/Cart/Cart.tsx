import React from 'react';
import { useSelector } from 'react-redux';

import * as cartReducer from '../../store/reducers/cart';
import Person from '../Person';
import Starship from '../Starship';

import './Cart.scss';

const Cart: React.FC = (): JSX.Element => {
  const cart: Array<any> = useSelector(cartReducer.state.cart.data);

  return (
    <main className="cart">
      <section className="container cart__container">
        {cart.map((item: any) => {
          if (item.type === 'person') {
            return (
              <Person
                person={item.item}
                isPointer={true}
                isInCart={true}
                key={item.item.name}
              />
            );
          }

          if (item.type === 'starship') {
            return (
              <Starship
                starship={item.item}
                isPointer={true}
                isInCart={true}
                key={item.item.name}
              />
            );
          }
        })}
      </section>
    </main>
  );
}

export default Cart;
