import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as cartReducer from '../../store/reducers/cart';
import { addToCart } from '../../store/actions/cart';
import { removeFromCart } from '../../store/actions/cart';

import './Starship.scss';

import { StarshipType } from './types';

import { useNavigate } from 'react-router-dom';

interface Props {
  starship: StarshipType;
  isPointer: boolean;
  isInCart?: boolean;
}

const Starship: React.FC<Props> = ({ starship, isPointer, isInCart }): JSX.Element => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const className = isPointer ? 'starship starship--pointer' : 'starship';
  const url = starship.url.split('/')[5];
  const isAddedToCart: boolean = useSelector(cartReducer.state.cart.data).some((item: any) => item.item.url === starship.url);

  return (
    <li
      className={className}
      key={starship.name}
    >
      <p
        className="starship__name"
        onClick={() => {
          navigate(`/starships/${url}`, {
            state: {
              starship
            }
          });
        }}
      >
        {starship.name}
      </p>
      <p>Model: {starship.model}</p>
      <p>Cargo capacity: {starship.cargo_capacity}</p>
      <p>Crew: {starship.crew}</p>
      <p>Hyperdrive rating: {starship.hyperdrive_rating}</p>

      {!isAddedToCart ? (
        <button
          type="button"
          className="starship__add-to-cart"
          onClick={() => {
            dispatch(addToCart.add({
              item: starship,
              type: 'starship'
            }));
          }}
        >
          Add to cart
        </button>
      ) : !isInCart ? (
        <p className="starship__added-to-cart">
          Added to the cart
        </p>
      ) : (
        <p
          className="starship__remove-from-cart"
          onClick={() => {
            dispatch(removeFromCart.remove({
              item: starship,
              type: 'starship'
            }));
          }}
        >
          Remove from the cart
        </p>
      )}
    </li>
  );
}

export default Starship;
