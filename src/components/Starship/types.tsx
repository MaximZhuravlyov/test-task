export type StarshipType = {
  name: string;
  model: string;
  cargo_capacity: string;
  crew: string;
  hyperdrive_rating: string;
  url: string;
};
