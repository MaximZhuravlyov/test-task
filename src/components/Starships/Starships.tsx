import React, { useState, useEffect } from 'react';

import './Starships.scss';

import { StarshipType } from '../../components/Starship/types';

import Starship from '../Starship';

interface Props {
  data: StarshipType[];
}

const Starships: React.FC<Props> = ({ data }): JSX.Element => {
  const [ mode, setMode ] = useState<string | null>('');

  let view: string | null = localStorage.getItem('starships-view') ? localStorage.getItem('starships-view') : 'table';

  useEffect(() => {
    setMode(view);
  }, []);

  return (
    <main className="starships">
      <section className="container starships__container">
        <ul className="starships__view">
          <li>
            <button
              className="starships__view-table"
              onClick={() => {
                setMode('table');
                localStorage.setItem('starships-view', 'table');
              }}
            >
              <i className={`starships__view-table-rectangle ${mode === 'table' ? ' starships__view-table-rectangle--active' : ''}`}></i>
              <i className={`starships__view-table-rectangle ${mode === 'table' ? ' starships__view-table-rectangle--active' : ''}`}></i>
              <i className={`starships__view-table-rectangle ${mode === 'table' ? ' starships__view-table-rectangle--active' : ''}`}></i>
              <i className={`starships__view-table-rectangle ${mode === 'table' ? ' starships__view-table-rectangle--active' : ''}`}></i>
            </button>
          </li>
          <li>
            <button
              className={`starships__view-list${mode === 'list' ? ' starships__view-list--active' : ''}`}
              onClick={() => {
                setMode('list');
                localStorage.setItem('starships-view', 'list');
              }}
            >
              <i className={`starships__view-list-line ${mode === 'list' ? ' starships__view-list-line--active' : ''}`}></i>
              <i className={`starships__view-list-line ${mode === 'list' ? ' starships__view-list-line--active' : ''}`}></i>
              <i className={`starships__view-list-line ${mode === 'list' ? ' starships__view-list-line--active' : ''}`}></i>
            </button>
          </li>
        </ul>
        <h1 className="starships__title">
          Starships
        </h1>
        <ul className={`starships__${mode}`}>
          {data.map((item: StarshipType) => (
            <Starship
              starship={item}
              isPointer={true}
              key={item.name}
            />
          ))}
        </ul>
      </section>
    </main>
  );
}

export default Starships;
