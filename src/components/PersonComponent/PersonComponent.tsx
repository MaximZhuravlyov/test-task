import React from 'react';
import { Link } from 'react-router-dom';
import Person from '../Person';
import { PersonType } from '../Person/types';

import './PersonComponent.scss';

interface Props {
  person: PersonType;
}

const PersonComponent: React.FC<Props> = ({ person }): JSX.Element => {
  return (
    <section className="person-details">
      <div className="container">
        <Link to="/people">Back</Link>
        <Person person={person} isPointer={false} />
      </div>
    </section>
  );
}

export default PersonComponent;
