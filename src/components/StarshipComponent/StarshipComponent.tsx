import React from 'react';
import { Link } from 'react-router-dom';
import Starship from '../Starship';
import { StarshipType } from '../Starship/types';

import './StarshipComponent.scss';

interface Props {
  starship: StarshipType;
}

const StarshipComponent: React.FC<Props> = ({ starship }): JSX.Element => {
  return (
    <section className="starship-details">
      <div className="container">
        <Link to="/starships">Back</Link>
        <Starship starship={starship} isPointer={false} />
      </div>
    </section>
  );
}

export default StarshipComponent;
