export const state = {
  cart: {
    data: (state: any) => state.cart.cart.data,
    status: (state: any) => state.cart.cart.status,
    error: (state: any) => state.cart.cart.error
  }
};
