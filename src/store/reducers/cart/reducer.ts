import * as types from '../../types';
import { initialState } from './initialState';

export const reducer = (state: any = initialState, action: any) => {
  switch (action.type) {
    case types.cart.ADD_TO_CART_SUCCESS:
      return {
        ...state,
        cart: {
          ...state.cart,
          status: 'success',
          data: [...state.cart.data, action.payload]
        }
      };

    case types.cart.ADD_TO_CART_FAILED:
      return {
        ...state,
        cart: {
          ...state.cart,
          status: 'failed',
          error: action.payload
        }
      };

    case types.cart.ADD_TO_CART_PENDING:
      return {
        ...state,
        cart: {
          ...state.cart,
          status: 'pending',
          error: null
        }
      };

    case types.cart.REMOVE_FROM_CART_SUCCESS:
      return {
        ...state,
        cart: {
          ...state.cart,
          status: 'success',
          data: state.cart.data.filter((item: any) => item.item.url !== action.payload.item.url)
        }
      };

    case types.cart.REMOVE_FROM_CART_FAILED:
      return {
        ...state,
        cart: {
          ...state.cart,
          status: 'failed',
          error: action.payload
        }
      };

    case types.cart.REMOVE_FROM_CART_PENDING:
      return {
        ...state,
        cart: {
          ...state.cart,
          status: 'pending',
          error: null
        }
      };

    case types.cart.RETRIEVE_ITEMS_SUCCESS:
      return {
        ...state,
        cart: {
          ...state.cart,
          status: 'pending',
          data: action.payload
        }
      };

    case types.cart.RETRIEVE_ITEMS_FAILED:
      return {
        ...state,
        cart: {
          ...state.cart,
          status: 'failed',
          error: action.payload
        }
      };

    case types.cart.RETRIEVE_ITEMS_PENDING:
      return {
        ...state,
        cart: {
          ...state.cart,
          status: 'pending',
          error: null
        }
      };

    default:
      return state;
  }
};
