import * as types from '../../types';
import {initialState} from './initialState';

export const reducer = (state: any = initialState, action: any) => {
  switch (action.type) {
    case types.starships.GET_STARSHIPS_SUCCESS:
      return {
        ...state,
        starships: {
          ...state.starships,
          status: 'success',
          data: action.payload
        }
      };

    case types.starships.GET_STARSHIPS_FAILED:
      return {
        ...state,
        starships: {
          ...state.starships,
          status: 'failed',
          error: action.payload
        }
      };

    case types.starships.GET_STARSHIPS_PENDING:
      return {
        ...state,
        starships: {
          ...state.starships,
          status: 'pending',
          error: null
        }
      };

    case types.starships.GET_STARSHIP_SUCCESS:
      return {
        ...state,
        starship: {
          ...state.starship,
          status: 'success',
          data: action.payload
        }
      };

    case types.starships.GET_STARSHIP_FAILED:
      return {
        ...state,
        starship: {
          ...state.starship,
          status: 'failed',
          error: action.payload
        }
      };

    case types.starships.GET_STARSHIP_PENDING:
      return {
        ...state,
        starship: {
          ...state.starship,
          status: 'pending',
          error: null
        }
      };

    default:
      return state;
  }
}
