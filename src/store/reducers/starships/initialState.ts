export const initialState = {
  starships: {
    data: null,
    error: null,
    status: null
  },
  starship: {
    data: null,
    error: null,
    status: null
  }
};
