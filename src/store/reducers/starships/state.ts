export const state = {
  starships: {
    data: (state: any) => state.starships.starships.data,
    status: (state: any) => state.starships.starships.status,
    error: (state: any) => state.starships.starships.error
  },

  starship: {
    data: (state: any) => state.starships.starship.data,
    status: (state: any) => state.starships.starship.data,
    error: (state: any) => state.starships.starship.data
  }
};
