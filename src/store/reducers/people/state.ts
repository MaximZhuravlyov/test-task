export const state = {
  people: {
    data: (state: any) => state.people.people.data,
    status: (state: any) => state.people.people.status,
    error: (state: any) => state.people.people.error
  },

  person: {
    data: (state: any) => state.people.person.data,
    status: (state: any) => state.people.person.status,
    error: (state: any) => state.people.person.error
  }
};
