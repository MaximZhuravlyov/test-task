import * as types from '../../types';
import { initialState } from './initialState';

export const reducer = (state: any = initialState, action: any) => {
  switch (action.type) {
    case types.people.GET_PEOPLE_SUCCESS:
      return {
        ...state,
        people: {
          ...state.people,
          status: 'success',
          data: action.payload
        }
      };

    case types.people.GET_PEOPLE_FAILED:
      return {
        ...state,
        people: {
          ...state.people,
          status: 'failed',
          error: action.payload
        }
      };

    case types.people.GET_PEOPLE_PENDING:
      return {
        ...state,
        people: {
          ...state.people,
          status: 'pending',
          error: null
        }
      };

    case types.people.GET_PERSON_SUCCESS:
      return {
        ...state,
        person: {
          ...state.person,
          status: 'success',
          data: action.payload
        }
      };

    case types.people.GET_PERSON_FAILED:
      return {
        ...state,
        person: {
          ...state.person,
          status: 'failed',
          error: action.payload
        }
      };

    case types.people.GET_PERSON_PENDING:
      return {
        ...state,
        person: {
          ...state.person,
          status: 'pending',
          error: null
        }
      };

    default:
      return state;
  }
}
