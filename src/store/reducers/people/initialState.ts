export const initialState = {
  people: {
    data: null,
    error: null,
    status: null
  },
  person: {
    data: null,
    error: null,
    status: null
  }
};
