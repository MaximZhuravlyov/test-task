import { combineReducers } from 'redux';
import { reducer as people } from './people/reducer';
import { reducer as starships } from './starships/reducer';
import { reducer as cart } from './cart/reducer';
import { viewReducer } from './view/viewReducer';

export const combineReducer = combineReducers({
  people,
  starships,
  cart,
  view: viewReducer
});
