const initialState = {
  view: ''
};

export const viewReducer = (state: any = initialState, action: any) => {
  if (action.type === 'SET_VIEW') {
    return {
      ...state,
      view: action.view
    };
  }

  return state;
};
