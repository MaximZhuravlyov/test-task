export const setView = (viewType: string) => {
  return async (dispatch: any, getState: any) => {
    const view = localStorage.getItem('view') ? localStorage.getItem('view') : viewType;

    dispatch({
      type: 'SET_VIEW',
      view
    });
  };
};
