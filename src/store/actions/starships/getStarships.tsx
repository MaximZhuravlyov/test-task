import * as types from '../../types';

export const getStarships = {
  get: () => {
    return async (dispatch: any, getState: any) => {
      dispatch(getStarships.getPending());

      return fetch('https://swapi.dev/api/starships')
        .then((response) => {
          if (response.status === 200) {
            return response
              .json()
              .then((json: any) => dispatch(getStarships.getSuccess(json.results))
              );
          } else {
            return response
              .json()
              .then((json: any) => dispatch(getStarships.getFailed(json))
              );
          }
        })
        .catch((response: any) => dispatch(getStarships.getFailed(response)));
    };
  },

  getSuccess: (response: any) => ({
    type: types.starships.GET_STARSHIPS_SUCCESS,
    payload: response
  }),

  getFailed: (response: any) => ({
    type: types.starships.GET_STARSHIPS_FAILED,
    payload: response
  }),

  getPending: () => ({
    type: types.starships.GET_STARSHIPS_PENDING
  })
};
