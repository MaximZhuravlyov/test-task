import * as types from '../../types';

export const getStarship = {
  get: (url: string) => {
    return async (dispatch: any, getState: any) => {
      dispatch(getStarship.getPending());

      return fetch(url)
        .then((response) => {
          if (response.status === 200) {
            return response
              .json()
              .then((json: any) => dispatch(getStarship.getSuccess(json))
              );
          } else {
            return response
              .json()
              .then((json: any) => dispatch(getStarship.getFailed(json))
              );
          }
        })
        .catch((response: any) => dispatch(getStarship.getFailed(response)));
    };
  },

  getSuccess: (response: any) => ({
    type: types.starships.GET_STARSHIP_SUCCESS,
    payload: response
  }),

  getFailed: (response: any) => ({
    type: types.starships.GET_STARSHIP_FAILED,
    payload: response
  }),

  getPending: () => ({
    type: types.starships.GET_STARSHIP_PENDING
  })
};
