import * as types from '../../types';

export const removeFromCart = {
  remove: (item: any) => {
    return (dispatch: any, getState: any) => {
      dispatch(removeFromCart.removePending());

      return dispatch(removeFromCart.removeSuccess(item));
    };
  },

  removeSuccess: (response: any) => ({
    type: types.cart.REMOVE_FROM_CART_SUCCESS,
    payload: response
  }),

  removeFailed: (response: any) => ({
    type: types.cart.REMOVE_FROM_CART_FAILED,
    payload: response
  }),

  removePending: () => ({
    type: types.cart.REMOVE_FROM_CART_PENDING
  })
};
