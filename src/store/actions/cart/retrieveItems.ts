import * as types from '../../types';

export const retrieveItems = {
  retrieve: () => {
    return (dispatch: any, getState: any) => {
      dispatch(retrieveItems.retrievePending());

      return dispatch(retrieveItems.retrieveSuccess(getState().cart.cart.data));
    };
  },

  retrieveSuccess: (response: any) => ({
    type: types.cart.RETRIEVE_ITEMS_SUCCESS,
    payload: response
  }),

  retrieveFailed: (response: any) => ({
    type: types.cart.RETRIEVE_ITEMS_FAILED,
    payload: response
  }),

  retrievePending: () => ({
    type: types.cart.RETRIEVE_ITEMS_PENDING
  })
};
