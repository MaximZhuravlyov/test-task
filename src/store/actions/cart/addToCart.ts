import * as types from '../../types';

export const addToCart = {
  add: (item: any) => {
    return (dispatch: any, getState: any) => {
      dispatch(addToCart.addPending());

      // const cart = localStorage.getItem('cart');

      // if (!cart) {
      //   localStorage.setItem('cart', JSON.stringify([ item ]));
      // } else {
      //   const array = JSON.parse(localStorage.getItem('cart') as string);
      //   array.push(item);
      //   localStorage.setItem('cart', JSON.stringify(array));
      // }

      return dispatch(addToCart.addSuccess(item));
    };
  },

  addSuccess: (response: any) => ({
    type: types.cart.ADD_TO_CART_SUCCESS,
    payload: response
  }),

  addFailed: (response: any) => ({
    type: types.cart.ADD_TO_CART_FAILED,
    payload: response
  }),

  addPending: () => ({
    type: types.cart.ADD_TO_CART_PENDING
  })
};
