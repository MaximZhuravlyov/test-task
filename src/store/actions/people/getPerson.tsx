import * as types from '../../types';

export const getPerson = {
  get: (url: string) => {
    return async (dispatch: any, getState: any) => {
      dispatch(getPerson.getPending());

      return fetch(url)
      .then((response) => {
        if (response.status === 200) {
          return response
            .json()
            .then((json: any) => dispatch(getPerson.getSuccess(json))
            );
        } else {
          return response
            .json()
            .then((json: any) => dispatch(getPerson.getFailed(json))
            );
        }
      })
      .catch((response: any) => dispatch(getPerson.getFailed(response)));
    };
  },

  getSuccess: (response: any) => ({
    type: types.people.GET_PERSON_SUCCESS,
    payload: response
  }),

  getFailed: (response: any) => ({
    type: types.people.GET_PERSON_FAILED,
    payload: response
  }),

  getPending: () => ({
    type: types.people.GET_PERSON_PENDING
  }),
};
