import * as types from '../../types';

export const getPeople = {
  get: () => {
    return async (dispatch: any, getState: any) => {
      dispatch(getPeople.getPending());

      return fetch('https://swapi.dev/api/people')
        .then((response) => {
          if (response.status === 200) {
            return response
              .json()
              .then((json: any) => dispatch(getPeople.getSuccess(json.results))
              );
          } else {
            return response
              .json()
              .then((json: any) => dispatch(getPeople.getFailed(json))
              );
          }
        })
        .catch((response: any) => dispatch(getPeople.getFailed(response)));
    };
  },

  getSuccess: (response: any) => ({
    type: types.people.GET_PEOPLE_SUCCESS,
    payload: response
  }),

  getFailed: (response: any) => ({
    type: types.people.GET_PEOPLE_FAILED,
    payload: response
  }),

  getPending: () => ({
    type: types.people.GET_PEOPLE_PENDING
  })
};
